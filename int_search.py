import pandas
import matplotlib.pyplot as plt

w_date = '2014-03-08'
m_date = '2014-11-19'

womensday = pandas.date_range(w_date,
                              periods=6,
                              freq=pandas.DateOffset(years=1))

mensday = pandas.date_range(m_date,
                            periods=5,
                            freq=pandas.DateOffset(years=1))


# --- Time series from Google trends
m_series = pandas.Series.from_csv('search_men_google_120319.csv', header=0)
w_series = pandas.Series.from_csv('search_women_google_120319.csv', header=0)
w_series.plot(label="Women's day", alpha=0.7, ls='dashed', c="orange")
m_series.plot(label="Men's day", c="green")

plt.title("Search term: International (see legend)")
plt.suptitle("Interest over time")

# --- Change axis ticks
labels = ["w" for _ in womensday]
labels.extend(["m" for _ in mensday])

locs = womensday.append(mensday)
plt.xticks(locs, labels)

[_i.set_color("red") for _i in plt.gca().get_xticklabels() if _i.get_text() == "w"]
[_i.set_color("blue") for _i in plt.gca().get_xticklabels() if _i.get_text() == "m"]

plt.legend()
plt.show()
