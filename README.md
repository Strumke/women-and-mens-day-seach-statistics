# Women and Mens day seach statistics

A small and crude comparison of search data from Google Trends (12-03-2019) for Google searches on "International Womens day" and "International Mens day". Frames made using Pandas, plots using matplotlib.pyplot. 

Usage: Download code and data files, and run

`python int_search.py`

You'll get a plot and that's it :-)

Nothing sinister implied here, I just think it's interesting.